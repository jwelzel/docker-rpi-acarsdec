# Docker Image based on Debian Stretch for Raspberry Pi with rtlsdr + acarsdec

### Entrypoint and Commandline
```
ENTRYPOINT ["/usr/local/bin/acarsdec"]
CMD ["-o 1 -N flightaimap:9999 -r 1 131.550 131.725 131.525"]
```

### Acarsdec usage
```
Usage: acarsdec  [-v] [-o lv] [-t time] [-A] [-n ipaddr:port] [-l logfile] [-g gain] [-p ppm] -r rtldevicenumber  f1 [f2] ... [fN]

 -v			: verbose
 -A			: don't display uplink messages (ie : only aircraft messages)

 -o lv			: output format : 0: no log, 1 one line by msg., 2 full (default) , 3 monitor mode, 4 newline separated JSON

 -t time			: set forget time (TTL) in seconds for monitor mode (default=600s)
 -l logfile		: Append log messages to logfile (Default : stdout).
 -n ipaddr:port		: send acars messages to addr:port on UDP in planeplotter compatible format
 -N ipaddr:port		: send acars messages to addr:port on UDP in acarsdec native format
 -j ipaddr:port		: send acars messages to addr:port on UDP in acarsdec json format
 -i stationid		: station id used in acarsdec network format.

 -g gain		: set rtl preamp gain in tenth of db (ie -g 90 for +9db). By default use AGC
 -p ppm			: set rtl ppm frequency correction
 -r rtldevice f1 [f2]...[f8]	: decode from rtl dongle number or S/N rtldevice receiving at VHF frequencies f1 and optionally f2 to f8 in Mhz (ie : -r 0 131.525 131.725 131.825 )
```

### Thanks
* To Frederik Granna for his [blog post](http://www.sysrun.io/2015/11/20/a-complete-docker-rpi-rtl-sdr-adsbacars-solution/) and some docker/code inspirations
* To Glenn Stewart for his [piaware containers](https://hub.docker.com/u/inodes/) 
