FROM jwelzel/rpi-rtlsdr-base:latest

MAINTAINER Jochen Welzel

RUN apt-get update && apt-get install \
    git-core \
    pkg-config \
    libusb-1.0-0 \
    libusb-1.0-0-dev

WORKDIR /tmp

RUN git clone https://github.com/TLeconte/acarsdec.git && \
    cd acarsdec && \
    make -f Makefile.rtl && \
    cp acarsdec /usr/local/bin

RUN apt-get remove git-core pkg-config libusb-1.0-0-dev && \
    apt-get clean && apt-get autoremove && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENTRYPOINT ["/usr/local/bin/acarsdec"]
CMD ["-o 1 -N flightaimap:9999 -r 1 131.550 131.725 131.525"]
